<?php

/**
*
*/
require_once('models/Product.php');

class ProductController
{

    function __construct()
    {
        // echo "constructor productController<br>";
    }

    public function index()
    {
        $products = Product::all();
        require('views/product/index.php');
    }

    public function create()
    {
        require('views/product/create.php');
    }

    public function store()
    {

        $product = new Product;

        $product->nombre = $_POST['nombre'];
        $product->precio = $_POST['precio'];
        $product->id_tipo = $_POST['id_tipo'];

        $product->fecha = DateTime::createFromFormat('Y-m-d H:i:s', $_POST['fecha']);
        $product->store();
        header('location:index');
    }

    public function edit($id)
    {
        $product = Product::find($id);
        include 'views/product/edit.php';
    }

}
