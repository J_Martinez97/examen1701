<?php require 'views/header.php'; ?>
<main>
    <div>

        <h1>Lista de productos</h1>

        <table>

            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Fecha</th>
                <th>Id_tipo</th>
                <th>Almacenamiento</th>
            </tr>
            <?php foreach ($producto as $product): ?>
                <tr>
                    <td><?php echo $product->id ?></td>
                    <td><?php echo $product->nombre ?></td>
                    <td><?php echo $product->precio ?></td>
                    <td><?php echo $product->id_tipo ?></td>
                    <td>
                        <a href="<?php
                        echo "edit/$product->id"
                        ?>">editar</a>
                    </td>
                    <td>
                        <a href="<?php
                        echo "visualizar/$product->id"
                        ?>">visitar</a>
                    </td>
                </tr>
            <?php endforeach ?>
        </table>

        <p>
        <a href="create">Nuevo</a>
        </p>
    </div>

</main>
<?php require 'views/footer.php'; ?>

