<?php

/**
*
*/
require('app/Model.php');

class Product extends Model
{
    public $id;
    public $nombre;
    public $precio;
    public $fecha;
    public $id_tipo;

    function __construct()
    {

    }


    public static function all()
    {
        $db = Product::connect();

        $stmt = $db->prepare("SELECT * FROM producto");
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Product');

        $results = $stmt->fetchAll();

        return $results;
    }

    public function store()
    {
        $db = $this->connect();

        $sql = "INSERT INTO producto (nombre, precio, fecha, id_tipo) VALUES(?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1, $this->name);
        $stmt->bindParam(2, $this->precio);
        $stmt->bindParam(3, $this->fecha->format('Y-m-d H:i:s'));
        $stmt->bindParam(2, $this->id_tipo);
        $result = $stmt->execute();
        return $result;
    }

    public function find($id)
    {
        $db = Product::connect();
        $sql = "SELECT * FROM producto WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Product');
        $result = $stmt->fetch();
        return $result;
    }

    public function save()
    {
        $db = $this->connect();
        $sql = "UPDATE product SET nombre=?, precio=?, fecha=? id_tipo=? WHERE id=?";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1, $this->nombre);
        $stmt->bindParam(2, $this->precio);
        $stmt->bindParam(3, $this->fecha);
        $stmt->bindParam(4, $this->id_tipo);
        $result = $stmt->execute();
        return $result;
    }

}
