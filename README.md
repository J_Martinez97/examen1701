# Examen Desarrollo Web en Entorno Servidor DWES1701

 Antes de empezar:
 - Inicia la captura de pantallas cada 5 segundos:
 usa chronolapse o el script que te facilita el
 profesor. El destino del mismo debe ser tu pendrive.
 -  Haz un fork del repositorio
 https://bitbucket.org/daw2rafa/examen1701
 - Clona desde tu máquina de trabajo el repositorio
  que acabas de crear en tu cuenta Bitbucket. Ubicalo
  en `/var/www/examen1701`
 - Por sencillez, edita la configuración de tu sitio
  web mvc17.local. Cambia el DocumentRoot a la
  ubicación
 - Puedes iniciar el examen. No olvides entregar tu
 USB con las capturas de pantalla.

## Enunciado.

En el examen hemos de hacer un CRUD sobre la tabla
_products_.

Deben usarse y modificarse si es preciso los ficheros
 `header.php` y `footer.php`

Asegura tu trabajo. Después de cada ejercicio haz un
commit.

1. 20P - Tabla de _products_. Ruta `/product/index`.
 La tabla debe incluir una columna de "acciones" con
 los enlaces necesarios.
2. COMMIT1
2. 20P - Creación de nuevos registros. Rutas
`/product/create` y `/product/store`
3. COMMIT2
4. 20P - Edición de registros.  Rutas
`/product/edit/{id}` y `/product/update/{id}`
5. COMMIT3
5. 20P - Guarda en sesión el nombre de los productos
visitados. Añade un enlace en la vista index a la
ruta `/product/visit/{id}`. La lista de nombres
guardados debe mostrarse en el __pie__ de todas las
 vistas.
6. COMMIT4
6. 5P - Haz que las fechas se lean y se muestren en
 formato dia/mes/año.
7. COMMIT5
7. 5P - Haz que el la tabla principal se mueste el
 nombre del tipo de producto en vez de si id.
8. COMMIT6
8. 5P - Haz que en el formulario de creación aparezca
 un select  con el nombre de los tipos de producto en
  vez de una caja de texto.
9. COMMIT7
9. 5P - Haz que en el formulario de edición  aparezca
 un select  con el nombre de los tipos de producto en
  vez de una caja de texto.
10. COMMIT8

Al terminar sube tu código a Bitbucket y haz un pull
request al profesor.
